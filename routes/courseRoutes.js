const express = require('express');

const router = express.Router();
const courseController = require('../controllers/courseControllers');
const auth = require('../auth')


// endpoint
router.post('/', auth.verify, courseController.addCourse);

// endpoint for getting all active courses
router.get('/allCourses',courseController.getAllActive);

// endpoint for getting a specific course
router.get('/:courseId', courseController.getCourse);


// endpoint update a specific course
router.put('/update/:courseId', auth.verify, courseController.updateCourse);

// endpoint archive a course
router.patch('/:courseId/archive', auth.verify, courseController.archiveCourse)

router.get('/archive/archivedCourses', courseController.getAllArchived);

module.exports = router


